import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:you_tap/entities/user.dart';
import 'package:you_tap/entities/base_response.dart';

part 'user_service.g.dart';

@RestApi(baseUrl: 'https://jsonplaceholder.typicode.com/')
abstract class UserService {
  factory UserService(Dio dio, {String baseUrl}) = _UserService;

  @GET('users')
  Future<BaseResponse<List<User>>> getAllUsers();

  @GET('users')
  Future<BaseResponse<List<User>>> getUsersById({
    @Query('id') required int id,
  });

  @GET('users')
  Future<BaseResponse<List<User>>> getUsersByName({
    @Query('name') required String name,
  });
}
