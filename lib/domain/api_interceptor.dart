import 'package:dio/dio.dart';

class ApiInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    Response response = Response(
        requestOptions: err.requestOptions,
        data: {'data': null, 'errorMessage': 'Something went wrong'});

    // send response with placeholder error message
    handler.resolve(response);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    List<dynamic> data = response.data;

    response.data = {'data': data, 'errorMessage': null};

    // wrap response in UserResponse to provide better error handling
    handler.resolve(response);
  }
}
