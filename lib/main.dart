import 'package:dio/dio.dart';
import 'package:dio_http_formatter/dio_http_formatter.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:you_tap/domain/api_interceptor.dart';
import 'package:you_tap/domain/services/user_service.dart';
import 'package:you_tap/presentation/user_list/screen/user_list_screen.dart';

void main() {
  runApp(const MyApp());
}

Logger logger = Logger(
  printer: PrettyPrinter(),
  output: ConsoleOutput(),
);

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<SingleChildWidget> _providers = [];
  late Dio _dio;

  @override
  void initState() {
    _dio = Dio();
    _dio.interceptors.clear();
    _dio.interceptors.add(ApiInterceptor());
    _dio.interceptors.add(HttpFormatter(logger: logger));

    _providers = [
      Provider<UserService>.value(
        value: UserService(_dio),
      )
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: _providers,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
        ),
        home: const UserListScreen(),
      ),
    );
  }
}
