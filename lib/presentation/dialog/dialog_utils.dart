import 'package:flutter/material.dart';
import 'package:you_tap/presentation/styles/app_text_styles.dart';

class DialogUtils {
  static String gameFinishedRouteName = '/gameFinished';
  static String errorRouteName = '/error';
  static String loadingRouteName = '/loading';

  static void showLoadingDialog(BuildContext context) => showDialog(
      routeSettings: RouteSettings(name: loadingRouteName),
      context: context,
      builder: (context) {
        return Container(
          color: Colors.transparent,
          width: 40,
          height: 40,
          child: const Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
            ),
          ),
        );
      });

  static void dismissLoadingDialog(BuildContext context) =>
      Navigator.of(context, rootNavigator: true).popUntil(
          (Route<dynamic> route) => route.settings.name != loadingRouteName);

  static void showGameFinishedDialog(
    BuildContext context, {
    bool cancellable = false,
    int totalScore = 0,
    Function()? callback,
  }) =>
      showDialog(
        routeSettings: RouteSettings(name: gameFinishedRouteName),
        useRootNavigator: true,
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
            clipBehavior: Clip.antiAlias,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Game Finished!',
                    style:
                        AppTextStyles.normal_700().copyWith(color: Colors.blue),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text('Total score: $totalScore'),
                  const SizedBox(
                    height: 18,
                  ),
                  MaterialButton(
                    onPressed: callback ?? () {},
                    child: const Text(
                      'Reset Game',
                      style: TextStyle(color: Colors.blue),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      );

  static void dismissGameFinishedDialog(BuildContext context) =>
      Navigator.of(context, rootNavigator: true).popUntil(
          (Route<dynamic> route) =>
              route.settings.name != gameFinishedRouteName);

  static void showErrorDialog(BuildContext context,
          {bool cancellable = true, String message = ''}) =>
      showDialog(
        routeSettings: RouteSettings(name: errorRouteName),
        useRootNavigator: true,
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
            clipBehavior: Clip.antiAlias,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Error!',
                    style:
                        AppTextStyles.normal_700().copyWith(color: Colors.red),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    message,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 18,
                  ),
                  MaterialButton(
                    onPressed: () {
                      dismissErrorDialog(context);
                    },
                    child: const Text(
                      'Dismiss',
                      style: TextStyle(color: Colors.blue),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      );

  static void dismissErrorDialog(BuildContext context) =>
      Navigator.of(context, rootNavigator: true).popUntil(
          (Route<dynamic> route) => route.settings.name != errorRouteName);
}
