import 'package:flutter/cupertino.dart';

class AppTextStyles {
  static TextStyle normal_700() =>
      const TextStyle(fontSize: 18, fontWeight: FontWeight.w700);

  static TextStyle title_700() =>
      const TextStyle(fontSize: 22, fontWeight: FontWeight.w700);
}
