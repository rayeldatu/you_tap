import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:you_tap/domain/services/user_service.dart';
import 'package:you_tap/presentation/dialog/dialog_utils.dart';
import 'package:you_tap/presentation/styles/app_text_styles.dart';
import 'package:you_tap/presentation/user_list/cubit/user_list_screen_cubit.dart';
import 'package:you_tap/presentation/user_list/cubit/user_list_screen_state.dart';
import 'package:you_tap/presentation/user_list/widgets/field.dart';
import 'package:you_tap/presentation/user_list/widgets/user_list_item.dart';

class UserListScreen extends StatefulWidget {
  const UserListScreen({Key? key}) : super(key: key);

  @override
  _UserListScreenState createState() => _UserListScreenState();
}

class _UserListScreenState extends State<UserListScreen> {
  TextEditingController searchByIdController = TextEditingController();
  TextEditingController searchByNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserListScreenCubit>(
        create: (BuildContext context) =>
            UserListScreenCubit(userService: context.read<UserService>())
              ..getAllUsers(),
        child: BlocConsumer<UserListScreenCubit, UserListScreenState>(
          listener: (context, state) {
            if (state is UserListScreenLoadingState) {
              DialogUtils.showLoadingDialog(context);
            } else {
              DialogUtils.dismissLoadingDialog(context);
            }
          },
          builder: (context, state) {
            UserListScreenCubit cubit = context.read<UserListScreenCubit>();
            return Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: Text(
                  'User List',
                  style: AppTextStyles.title_700(),
                ),
              ),
              body: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        Field(
                          controller: searchByIdController,
                          hintText: 'Search by ID (1, 2, 3)',
                          labelText: 'ID',
                          onFieldSubmitted: (text) {
                            if (text.isEmpty) {
                              cubit.getAllUsers();
                              return;
                            }
                            cubit.getAllUsersById(id: int.parse(text));
                          },
                          suffixWidget: IconButton(
                              onPressed: () {
                                if (searchByIdController.text.isEmpty) {
                                  cubit.getAllUsers();
                                  return;
                                }
                                cubit.getAllUsersById(
                                    id: int.parse(searchByIdController.text));
                              },
                              icon: const Icon(Icons.search)),
                        ),
                        Field(
                          controller: searchByNameController,
                          hintText: 'Search by Name (John Smith)',
                          labelText: 'Name',
                          onFieldSubmitted: (text) {
                            if (text.isEmpty) {
                              cubit.getAllUsers();
                              return;
                            }
                            cubit.getAllUsersByName(name: text);
                          },
                          suffixWidget: IconButton(
                              onPressed: () {
                                if (searchByNameController.text.isEmpty) {
                                  cubit.getAllUsers();
                                  return;
                                }

                                cubit.getAllUsersByName(
                                    name: searchByNameController.text);
                              },
                              icon: const Icon(Icons.search)),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Expanded(
                    child: state.viewModel.users.isEmpty
                        ? const Center(
                            child: Text('Nothing to show'),
                          )
                        : ListView.separated(
                            itemCount: state.viewModel.users.length,
                            itemBuilder: (context, index) => UserListItem(
                                user: state.viewModel.users[index]),
                            separatorBuilder: (context, index) =>
                                const Divider(),
                          ),
                  ),
                ],
              ),
            );
          },
        ));
  }
}
