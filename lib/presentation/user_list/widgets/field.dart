import 'package:flutter/material.dart';

class Field extends StatelessWidget {
  const Field(
      {Key? key,
      this.controller,
      this.labelText,
      this.hintText,
      this.textInputType = TextInputType.text,
      this.onFieldSubmitted,
      this.suffixWidget})
      : super(key: key);

  final TextEditingController? controller;
  final String? labelText;
  final String? hintText;
  final TextInputType? textInputType;
  final Widget? suffixWidget;
  final Function(String)? onFieldSubmitted;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        controller: controller,
        textInputAction: TextInputAction.done,
        onFieldSubmitted: onFieldSubmitted,
        keyboardType: textInputType,
        decoration: InputDecoration(
          hintText: hintText,
          labelText: labelText,
          suffix: suffixWidget,
        ));
  }
}
