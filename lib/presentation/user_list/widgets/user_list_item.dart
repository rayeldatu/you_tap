import 'package:flutter/material.dart';
import 'package:you_tap/entities/user.dart';

class UserListItem extends StatelessWidget {
  const UserListItem({Key? key, required this.user}) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text('ID: ${user.id ?? 'N/A'}'),
        const SizedBox(
          height: 4,
        ),
        Text('EMAIL: ${user.email ?? 'N/A'}'),
        const SizedBox(
          height: 4,
        ),
        Text('PHONE: ${user.phone ?? 'N/A'}')
      ],
    );
  }
}
