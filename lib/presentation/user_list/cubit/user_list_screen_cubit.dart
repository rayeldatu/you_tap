import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:you_tap/domain/services/user_service.dart';
import 'package:you_tap/entities/user.dart';
import 'package:you_tap/entities/base_response.dart';
import 'package:you_tap/presentation/user_list/cubit/user_list_screen_state.dart';
import 'package:you_tap/presentation/user_list/view_model/user_list_screen_view_model.dart';

class UserListScreenCubit extends Cubit<UserListScreenState> {
  UserListScreenCubit({required this.userService})
      : super(UserListScreenState.initial(UserListScreenViewModel()));

  final UserService userService;

  Future<void> getAllUsers() async {
    emit(UserListScreenLoadingState(state.viewModel));
    BaseResponse<List<User>> users = await userService.getAllUsers();

    if (users.error != null) {
      emit(
          UserListScreenState.fetchFail(state.viewModel, users.error?.message));
      return;
    }

    state.viewModel.users = users.data ?? [];

    emit(UserListScreenState.fetchSuccess(state.viewModel));
  }

  Future<void> getAllUsersById({required int id}) async {
    emit(UserListScreenLoadingState(state.viewModel));
    BaseResponse<List<User>> users = await userService.getUsersById(id: id);

    if (users.error != null) {
      emit(
          UserListScreenState.fetchFail(state.viewModel, users.error?.message));
      return;
    }

    state.viewModel.users = users.data ?? [];

    emit(UserListScreenState.fetchSuccess(state.viewModel));
  }

  Future<void> getAllUsersByName({required String name}) async {
    emit(UserListScreenLoadingState(state.viewModel));
    BaseResponse<List<User>> users =
        await userService.getUsersByName(name: name);

    if (users.error != null) {
      emit(
          UserListScreenState.fetchFail(state.viewModel, users.error?.message));
      return;
    }

    state.viewModel.users = users.data ?? [];

    emit(UserListScreenState.fetchSuccess(state.viewModel));
  }
}
