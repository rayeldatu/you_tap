import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:you_tap/presentation/user_list/view_model/user_list_screen_view_model.dart';

part 'user_list_screen_state.freezed.dart';

@freezed
class UserListScreenState with _$UserListScreenState {
  factory UserListScreenState.initial(UserListScreenViewModel viewModel) =
      UserListScreenInitialState;
  factory UserListScreenState.loading(UserListScreenViewModel viewModel) =
      UserListScreenLoadingState;
  factory UserListScreenState.fetchSuccess(UserListScreenViewModel viewModel) =
      UserListScreenFetchSuccessState;
  factory UserListScreenState.fetchFail(
          UserListScreenViewModel viewModel, String? message) =
      UserListScreenFetchFailState;
}
