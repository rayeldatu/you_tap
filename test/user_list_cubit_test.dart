import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:you_tap/domain/services/user_service.dart';
import 'package:you_tap/entities/base_response.dart';
import 'package:you_tap/entities/user.dart';
import 'package:you_tap/presentation/user_list/cubit/user_list_screen_cubit.dart';
import 'package:you_tap/presentation/user_list/cubit/user_list_screen_state.dart';

import 'user_list_cubit_test.mocks.dart';

@GenerateMocks([UserService])
main() {
  late UserService userService;
  late UserListScreenCubit cubit;

  setUp(() {
    userService = MockUserService();

    cubit = UserListScreenCubit(userService: userService);
  });

  group('Cubit Success Tests', () {
    test('success getAllUsers', () async {
      BaseResponse<List<User>> baseResponse = BaseResponse([User()], null);
      when(userService.getAllUsers())
          .thenAnswer((realInvocation) => Future.value(baseResponse));

      expectLater(
          cubit.stream.asBroadcastStream(),
          emitsInOrder([
            UserListScreenState.loading(cubit.state.viewModel),
            UserListScreenState.fetchSuccess(cubit.state.viewModel)
          ]));

      await cubit.getAllUsers();

      expect(cubit.state.viewModel.users, isNotNull);
      expect(cubit.state.viewModel.users, isNotEmpty);
      expect(cubit.state is! UserListScreenFetchFailState, true);
    });

    test('success getAllUsersById', () async {
      const int id = 1;
      BaseResponse<List<User>> baseResponse =
          BaseResponse([User(id: id)], null);

      when(userService.getUsersById(id: 1))
          .thenAnswer((realInvocation) => Future.value(baseResponse));

      expectLater(
          cubit.stream.asBroadcastStream(),
          emitsInOrder([
            UserListScreenState.loading(cubit.state.viewModel),
            UserListScreenState.fetchSuccess(cubit.state.viewModel)
          ]));

      await cubit.getAllUsersById(id: 1);

      expect(cubit.state.viewModel.users, isNotNull);
      expect(cubit.state.viewModel.users, isNotEmpty);

      // All users should have id == 1
      expect(
          cubit.state.viewModel.users
              .where((element) => element.id != 1)
              .toList(),
          isEmpty);
      expect(cubit.state is! UserListScreenFetchFailState, true);
    });

    test('success getAllUsersByName', () async {
      const String name = 'Sample Name';
      BaseResponse<List<User>> baseResponse =
          BaseResponse([User(name: name)], null);

      when(userService.getUsersByName(name: name))
          .thenAnswer((realInvocation) => Future.value(baseResponse));

      expectLater(
          cubit.stream.asBroadcastStream(),
          emitsInOrder([
            UserListScreenState.loading(cubit.state.viewModel),
            UserListScreenState.fetchSuccess(cubit.state.viewModel)
          ]));

      await cubit.getAllUsersByName(name: name);

      expect(cubit.state.viewModel.users, isNotNull);
      expect(cubit.state.viewModel.users, isNotEmpty);

      // All users should have name == 'Sample Name'
      expect(
          cubit.state.viewModel.users
              .where((element) => element.name != name)
              .toList(),
          isEmpty);
      expect(cubit.state is! UserListScreenFetchFailState, true);
    });
  });

  group('Cubit Error handling test', () {
    const String errorMessage = 'sample error message';
    BaseResponse<List<User>> errorResponse =
        BaseResponse(null, Error(errorMessage));
    test('fail getAllUsers', () async {
      when(userService.getAllUsers())
          .thenAnswer((realInvocation) => Future.value(errorResponse));

      expectLater(
          cubit.stream.asBroadcastStream(),
          emitsInOrder([
            UserListScreenState.loading(cubit.state.viewModel),
            UserListScreenState.fetchFail(
                cubit.state.viewModel, errorResponse.error!.message)
          ]));

      await cubit.getAllUsers();

      expect(cubit.state is UserListScreenFetchFailState, true);
      expect(cubit.state is! UserListScreenFetchSuccessState, true);
      expect((cubit.state as UserListScreenFetchFailState).message,
          equals(errorMessage));
    });

    test('fail getAllUsersById', () async {
      const int id = 1;

      when(userService.getUsersById(id: id))
          .thenAnswer((realInvocation) => Future.value(errorResponse));

      expectLater(
          cubit.stream.asBroadcastStream(),
          emitsInOrder([
            UserListScreenState.loading(cubit.state.viewModel),
            UserListScreenState.fetchFail(
                cubit.state.viewModel, errorResponse.error!.message)
          ]));

      await cubit.getAllUsersById(id: id);

      expect(cubit.state is UserListScreenFetchFailState, true);
      expect(cubit.state is! UserListScreenFetchSuccessState, true);
      expect((cubit.state as UserListScreenFetchFailState).message,
          equals(errorMessage));
    });

    test('fail getAllUsersByName', () async {
      const String name = 'Sample Name';

      when(userService.getUsersByName(name: name))
          .thenAnswer((realInvocation) => Future.value(errorResponse));

      expectLater(
          cubit.stream.asBroadcastStream(),
          emitsInOrder([
            UserListScreenState.loading(cubit.state.viewModel),
            UserListScreenState.fetchFail(
                cubit.state.viewModel, errorResponse.error!.message)
          ]));

      await cubit.getAllUsersByName(name: name);

      expect(cubit.state is UserListScreenFetchFailState, true);
      expect(cubit.state is! UserListScreenFetchSuccessState, true);
      expect((cubit.state as UserListScreenFetchFailState).message,
          equals(errorMessage));
    });
  });
}
